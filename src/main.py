# main.py
#
# Copyright 2023 Danial Behzadi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import json
import sys
import socket
import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')

from gi.repository import Gtk, Gio, Adw
from .window import KoriWindow


class KoriApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self):
        super().__init__(application_id='rocks.gnu.kori',
                         flags=Gio.ApplicationFlags.DEFAULT_FLAGS)
        self.create_action('quit', lambda *_: self.quit(), ['<primary>q'])
        self.create_action('about', self.on_about_action)
        self.create_action('preferences', self.on_preferences_action)
        self.create_action('Up', self.on_input_action)
        self.create_action('Down', self.on_input_action)
        self.create_action('Right', self.on_input_action)
        self.create_action('Left', self.on_input_action)
        self.create_action('Select', self.on_input_action)

    def on_input_action(self, *argv, **kwargs):
        name = (argv[0].get_name())
        print(name)
        HOST, PORT = "192.168.1.253", 9090

        match name:
            case "play":
                request = {
                    "jsonrpc": "2.0",
                    "method": "Player.PlayPause",
                    "params": {"playerid": 1},
                    "id": 1,
                }
            case "Right" | "Left" | "Up" | "Down" | "Select":
                request = {
                    "jsonrpc": "2.0",
                    "method": f"Input.{name}",
                    "id": 1,
                }
            case _:
                raise TypeError("Wrong input")

        request = json.dumps(request)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            s.connect((HOST, PORT))
            s.sendall(bytes(request, encoding="utf-8"))
            received = s.recv(1024)
        finally:
            s.close()


    def do_activate(self):
        """Called when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        win = self.props.active_window
        if not win:
            win = KoriWindow(application=self)
        win.present()

    def on_about_action(self, widget, _):
        """Callback for the app.about action."""
        about = Adw.AboutWindow(transient_for=self.props.active_window,
                                application_name='kori',
                                application_icon='rocks.gnu.kori',
                                developer_name='Danial Behzadi',
                                version='0.1.0',
                                developers=['Danial Behzadi'],
                                copyright='© 2023 Danial Behzadi')
        about.present()

    def on_preferences_action(self, widget, _):
        """Callback for the app.preferences action."""
        print('app.preferences action activated')

    def create_action(self, name, callback, shortcuts=None):
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
        """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    """The application's entry point."""
    app = KoriApplication()
    return app.run(sys.argv)
